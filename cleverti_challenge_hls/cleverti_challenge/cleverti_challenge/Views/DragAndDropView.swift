//
//  DragAndDropView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

class DragAndDropView: UIView {
    
    var childView : UIView
    var dragGesture : UIPanGestureRecognizer?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, view: UIView) {
        childView = view
        super.init(frame: frame)
        childView.frame = bounds
        
        dragGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedView(_:)))
        addGestureRecognizer(dragGesture!)
        dragGesture?.delegate = self
        
        addSubview(childView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        childView.frame = bounds
    }
    
    let swipeVelocity: CGFloat = 1500
    @objc func draggedView(_ sender: UIPanGestureRecognizer)  {
        //Move view
        
        let gestureEnded = sender.state == UIGestureRecognizerState.ended
        let velocity = sender.velocity(in: self)
        
        if gestureEnded && (abs(velocity.y) > swipeVelocity || abs(velocity.x) > swipeVelocity){
            handlePanAsSwipe(withVelocity: velocity)
        } else {
            move(basedOnPan: sender)
        }
    }
    
    func move(basedOnPan panGesture : UIPanGestureRecognizer)  {
        let translation = panGesture.translation(in: superview)
        center = CGPoint(x: center.x + translation.x, y: center.y + translation.y)
        panGesture.setTranslation(CGPoint.zero, in: superview)
    }
    
    func handlePanAsSwipe(withVelocity velocity: CGPoint)  {
        print("swipe")
        print("velocity: (\(velocity.x),(\(velocity.y))")
        var direction : UISwipeGestureRecognizerDirection?
        
        //find direction
        if abs(velocity.x) > abs(velocity.y) {
            //horizontal axis
            direction = velocity.x < 0 ? .left : .right
        } else {
            //vertical axis
            direction = velocity.y < 0 ? .up : .down
        }
        
        var landingPoint : CGPoint?
        switch direction! {
        case .up:
            print("up")
            landingPoint = CGPoint(x: frame.origin.x, y: 0)
        case .down:
            print("down")
            landingPoint = CGPoint(x: frame.origin.x, y: superview!.frame.height - bounds.width)
        case .right:
            print("right")
            landingPoint = CGPoint(x: superview!.frame.width - frame.height, y: frame.origin.y)
        case .left:
            print("left")
            landingPoint = CGPoint(x: 0, y: frame.origin.y)
        default:
            print("Now we have swipes in 3 dimentions? Some things is wrong investigate")
        }
        
        
        UIView.animate(withDuration: 0.30){
//            let newFrame = CGRect(origin: CGPoint(x: clampedLandingPositionX, y: clampedLandingPositionY),
//                                  size: self.bounds.size)
            let newFrame = CGRect(origin: landingPoint!, size: self.bounds.size)
            self.frame  = newFrame
        }
        
    }
    
    func move(toEdge direction:UISwipeGestureRecognizerDirection )  {
        var landingPoint : CGPoint?
        switch direction {
        case .up:
            print("up")
            landingPoint = CGPoint(x: frame.origin.x, y: 0)
        case .down:
            print("down")
            landingPoint = CGPoint(x: frame.origin.x, y: superview?.frame.height ?? 0)
        case .right:
            print("right")
            landingPoint = CGPoint(x: 0, y: superview?.frame.width ?? 0)
        case .left:
            print("left")
            landingPoint = CGPoint(x: 0, y: 0)
        default:
            print("Now we have swipes in 3 dimentions? Some things is wrong investigate")
        }
        guard let _ = landingPoint else { return }
        print("point (\(landingPoint?.x.description ?? "nil") , \(landingPoint?.y.description ?? "nil"))" )
        UIView.animate(withDuration: 0) {
            let newFrame = CGRect(origin: landingPoint!, size: self.bounds.size)
            self.frame  = newFrame
        }
    }
    
    @objc func swippedView(_ sender: UISwipeGestureRecognizer){
        move(toEdge: sender.direction)
    }
}

extension DragAndDropView : UIGestureRecognizerDelegate
{
    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //        guard let _ = gestureRecognizer as? UISwipeGestureRecognizer,
    //            let _ = otherGestureRecognizer as? UIPanGestureRecognizer else {
    //                return true
    //        }
    //        return false
    //    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //        guard let _ = gestureRecognizer as? UISwipeGestureRecognizer,
    //            let _ = otherGestureRecognizer as? UIPanGestureRecognizer else {
    //            return true
    //        }
    //        return false
    //    }
}


