//
//  FileDownloader.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 05/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import Pantomime

class HLSFileDownloader: NSObject {
    var completed: ((URL?) -> Void)?
    var progress: ((Float) -> Void)?
    
    let sessionConfig = URLSessionConfiguration.background(withIdentifier: "dn.identifier")
    var session: URLSession? = nil
    var totalDownloadedBytes: Int64 = 0
    var audioFileUrl: URL?
    var outputStream: OutputStream?
    let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("mergeFile", isDirectory: false)
    var totalSegments: Int?
    var segmentsCount: Int = 0
    let serialQueue = DispatchQueue(label: "com.queue.Serial")
    
    func downloadFileWithHighestQuality(urlString: String,
                                        progress: @escaping (Float) -> Void,
                                        completed: @escaping (URL?) -> Void)  {
        
        
        self.completed = completed
        self.progress = progress
        
        let url = URL(string: urlString)!
        
        let builder = ManifestBuilder()
        let manifest = builder.parse(url)
        
        let playlistCount = manifest.getPlaylistCount()
        guard playlistCount > 0 else {
            progress(1)
            self.completed?(nil)
            return
        }
        
        var highestQualityMediaPlaylist = manifest.getPlaylist(0)!
        
        for idx in 1..<playlistCount {
            
            let mediaPlaylist = manifest.getPlaylist(idx)!
            if  mediaPlaylist.bandwidth > highestQualityMediaPlaylist.bandwidth
            {
                highestQualityMediaPlaylist = mediaPlaylist
            }
            
            print("\(idx) \(String(describing: mediaPlaylist.path)) \(mediaPlaylist.bandwidth)")
            print("segment count \(String(describing: mediaPlaylist.getSegmentCount()))")
        }
        
        print("end")
        print(("highest qual \(highestQualityMediaPlaylist.path!)"))
        
        audioFileUrl = url.deletingLastPathComponent().appendingPathComponent(highestQualityMediaPlaylist.path!)
        
        sessionConfig.httpMaximumConnectionsPerHost = 2
        session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: OperationQueue())
        
        totalSegments = highestQualityMediaPlaylist.getSegmentCount()
        
        //create file
        if FileManager.default.fileExists(atPath: filePath.absoluteString) {
            try! FileManager.default.removeItem(at: filePath)
        }
        FileManager.default.createFile(atPath: filePath.absoluteString, contents: nil, attributes: nil)
        outputStream = OutputStream(toFileAtPath: filePath.absoluteString, append: true)
        outputStream?.open()
        
        for i in 0..<totalSegments! {
            
            let tsName = highestQualityMediaPlaylist.getSegment(i)?.path
            
            let downloadUrl = url.deletingPathExtension().appendingPathExtension(tsName!)
            print(downloadUrl)
            let request = URLRequest(url: downloadUrl)
            let downloadTask = session?.downloadTask(with: request)
            downloadTask?.resume()
        }
        
    }
    
    func merge(file: URL, chunkSize: Int = 1000000) throws {
        
        
        let reader = try FileHandle(forReadingFrom: file)
        var data = reader.readData(ofLength: chunkSize)
        
        
        
        
        while data.count > 0 {
            _ = data.withUnsafeBytes { (bytes: UnsafePointer<UInt8>) -> Int in
                var pointer = bytes
                var bytesRemaining = data.count
                var totalBytesWritten = 0
                
                while bytesRemaining > 0 {
                    let bytesWritten = outputStream!.write(pointer, maxLength: bytesRemaining)
                    if bytesWritten < 0 {
                        return -1
                    }
                    
                    bytesRemaining -= bytesWritten
                    pointer += bytesWritten
                    totalBytesWritten += bytesWritten
                }
                
                return totalBytesWritten
            }
            data = reader.readData(ofLength: chunkSize)
        }
        reader.closeFile()
    }
}

extension HLSFileDownloader: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        serialQueue.sync {
            try! merge(file: location)
            try! FileManager.default.removeItem(at: location)
            segmentsCount += 1
            let progressPercentage = Float(Float(self.segmentsCount) / Float(self.totalSegments!));
            DispatchQueue.main.async {self.progress?(progressPercentage)}
            if segmentsCount >= totalSegments! {
                outputStream?.close()
                DispatchQueue.main.async {self.completed?(self.filePath)}
            }
        }
        
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        print("error")
        DispatchQueue.main.async {self.completed?(nil)}
    }
    
    
    
}
