//
//  PlayerStateViewProtocol.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

protocol PlayerStateView : AnyObject {
    var view: UIView { get }
    func setup()
    func cleanup()
}
