//
//  PlayerFetchingView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

class PlayerFetchingView : PlayerStateView {
    let _view: UIView
    let _filledView: UIView // View that indicates the progress of the download
    let _filledViewHeightConstraint: NSLayoutConstraint
    
    let fileUrl: String
    let fetchCompleted: (URL?) -> Void
    let fileDownloader : HLSFileDownloader
    
    var view : UIView {return _view}
    
    init(fileUrl: String, onFetchCompleted:@escaping (URL?) -> Void, fillColor: UIColor = UIColor.red) {
        self.fileUrl = fileUrl
        self.fetchCompleted = onFetchCompleted
        
        fileDownloader = HLSFileDownloader();
        
        _view = UIView()
        _filledView = UIView()
        _filledView.backgroundColor = fillColor
        _view.addSubview(_filledView)
        
        _filledView.translatesAutoresizingMaskIntoConstraints = false;
        _filledViewHeightConstraint = NSLayoutConstraint(item: _filledView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        let filledViewBottomConstraint = NSLayoutConstraint(item: _filledView, attribute: .bottom, relatedBy: .equal, toItem: _view, attribute: .bottom, multiplier: 1, constant: 0)
        let filledViewLeftConstraint = NSLayoutConstraint(item: _filledView, attribute: .left, relatedBy: .equal, toItem: _view, attribute: .left, multiplier: 1, constant: 0)
        let filledViewRightConstraint = NSLayoutConstraint(item: _filledView, attribute: .right, relatedBy: .equal, toItem: _view, attribute: .right, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([_filledViewHeightConstraint,
                                     filledViewLeftConstraint,
                                     filledViewRightConstraint,
                                     filledViewBottomConstraint])
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        _view.addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        let centerXConstraint = NSLayoutConstraint(item: spinner, attribute: .centerX, relatedBy: .equal, toItem: _view, attribute: .centerX, multiplier: 1, constant: 0)
        let centerYConstraint = NSLayoutConstraint(item: spinner, attribute: .centerY, relatedBy: .equal, toItem: _view, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([centerXConstraint, centerYConstraint])
        spinner.startAnimating()
    }
    
    func setup() {
        setProgress(percentage: 0.0)
        
        fileDownloader.downloadFileWithHighestQuality(urlString: fileUrl, progress: { (progress) in
            self.setProgress(percentage: CGFloat(progress))
        }) { (url) in
            self.fetchCompleted(url)
        }
    }
    
    func cleanup() {
        view.removeFromSuperview()
    }
    
    //Zero progress 0% => 0.0
    //Progress completed 100% => 1.0
    func setProgress(percentage: CGFloat )  {
        guard percentage >= 0.0 || percentage <= 1.0 else {
            print("Percentage should be a value between 0.0 and 1.0")
            return
        }
        print("Set progress at \(percentage)")
        UIView.animate(withDuration: 0.3) {
            self._filledViewHeightConstraint.constant = self.view.bounds.height * percentage
        }
    }
}
