//
//  DummyStateView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

class DummyStateView : PlayerStateView {
    let _view :UIView
    
    var view: UIView { return _view }
    
    init() {
        _view = UIView()
        _view.backgroundColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
    }
    
    func setup() {
        // No implementation needed
    }
    
    func cleanup() {
        view.removeFromSuperview()
    }
}
