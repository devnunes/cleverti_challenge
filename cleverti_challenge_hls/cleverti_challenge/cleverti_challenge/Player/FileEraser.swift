//
//  FileEraser.swift
//  cleverti_challenge
//
//  Created by Diogo Nunes on 06/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
class FileEraser {
    func erase(url: URL, onCompleted completed: ()->Void)  {
        do {
            try FileManager.default.removeItem(at: url)
        }catch {
            debugPrint("Error:\(error.localizedDescription)")
        }
        completed()
    }
}
