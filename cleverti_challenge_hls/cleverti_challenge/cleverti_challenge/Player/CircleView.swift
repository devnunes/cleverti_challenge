//
//  CircleView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

class CircleView : UIView {
    
    var contentView : UIView = UIView()
    var childView : UIView?
    
    
    override init(frame: CGRect) {
        self.contentView = UIView()
        super.init(frame:frame)
        
        //TODO remove this! This is only for debug
        backgroundColor = UIColor.red
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupChildView(_ view: UIView )  {
        //clean up if necessarys
        childView?.removeFromSuperview()
        childView = nil
        contentView.removeFromSuperview()
        
        //setup new child view
        
        contentView.frame = bounds;
        addSubview(contentView);
        
        view.frame = contentView.bounds;
        contentView.addSubview(view)
        childView = view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.width / 2
        layer.masksToBounds = true
        contentView.frame = bounds
        
        if let aChildView = childView {
            aChildView.frame = contentView.bounds
        }
    }
}
