//
//  PlayerView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

enum PlayerState : String {
    case uninitialized
    case fetching
    case playing
    case completed
}

class PlayerView : CircleView {
    
    //current state of playback
    var state = PlayerState.uninitialized
    var stateView : PlayerStateView?
    var tapGesture : UITapGestureRecognizer?
    var fileUrl: String?
    var storedFile: URL?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    convenience init(frame: CGRect, fileUrl: String) {
        self.init(frame: frame)
        self.fileUrl = fileUrl
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let view = stateView?.view {
            view.frame = bounds
        }
    }
    
    func initialize() {
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        addGestureRecognizer(tapGesture!)
        backgroundColor = UIColor.clear
        change(toState: .uninitialized)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer)  {
        switch state {
        case .uninitialized:
            change(toState: .fetching)
        case .fetching:
            break
        case .playing:
            if let playStateView = self.stateView as?  PlayerPlayView {
                playStateView.togglePause()
            }
        case .completed:
            break
        }
    }
    
    func change(toState newState: PlayerState )  {
        stateView?.cleanup()
        state = newState
        stateView = createView(for: state)
        let view = stateView!.view
        stateView?.setup()
        view.frame = bounds
        addSubview(view)
    }
    
    func createView(for state : PlayerState) -> PlayerStateView {
        switch state {
        case .uninitialized:
            return PlayerUninitializedView()
        case .fetching:
            return PlayerFetchingView(fileUrl:fileUrl!, onFetchCompleted: { url in
                self.storedFile = url
                self.change(toState: .playing)
            })
        case .playing:
            return PlayerPlayView(filePath: "sample_sound.mp3") {
                self.change(toState: .completed)
            }
        case .completed:
            return PlayerCompletedView(fileToDelete: storedFile!) {
                self.change(toState: .uninitialized)
            }
        }
    }
    
}


